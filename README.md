# wisecp-knowledge-base

This read me file contains the information that we learned while working on the wisecp project.


## How to send email in wisecp

```
Modules::Load("Mail");
$mail_module  = Config::get("modules/mail");
$mail         = $mail_module && $mail_module != "none" ? new $mail_module() : false;
if($mail)
{
    $message      = 'test message body, html body test';
    $address      = 'test@gmail.com';
    $name        = 'John Smith';
    $mail->subject("Subject heading here");
    $send   = $mail->body($message);
    $send   = $send->addAddress($address,$name)->submit();
    if($send)
    {
        echo "Message Sent!";
        LogManager::Mail_Log(0,"another",$mail->getSubject(),$mail->getBody(),implode(",",$mail->getAddresses()));
    }
    else
    {
        echo "Error Message: ".$mail->error;
    }
}
else
{
    echo "Not selected mail module.";
}
```


## how to use date conversion

We can get the default saved date format in wisecp by `Config::get("options/date-format")`

###Convert `Y-m-d H:i:s` into wisecp date format

`DateManager::format(Config::get("options/date-format")." - H:i:s", date("Y-m-d H:i:s"));`


###Convert `d-m-Y H:i:s` date into `Y-m-d H:i:s`

`DateManager::format("Y-m-d H:i:s", date("d-m-Y H:i:s"));`

###Convert date format into another

`DateManager::format($dateFormatConvertTo, $dateFormatConvertFrom);`



## Create Custom Page in WISECP

In wisecp you can't create a custom page on the root or any other place and include init.php, It will always give a 404. we can't create a page in that way.

if we need to create a custom page in wisecp. we can create it in `/public_html/templates/website/{$sitetemplate}/pages`. 
`classic` is the default site template in wisecp.



## Errors

In Wisecp there was no setting for error display in admin panel like in WHMCS. Wisecp stores most of the configuration information in the php files.

we can enable error display in wisecp in the file `/public_html/coremio/configuration/constants.php`.
we need to change `DEVELOPMENT` and `ERROR_DEBUG` to true.



## Ajax

In Wisecp to send ajax requests in clientarea and admin area Wisecp uses the `MioAjax` function.
We can see these function definations here to have a better understanding before using them

In admin panel `/public_html/resources/assets/javascript/admio.js`
In client area `/public_html/resources/assets/javascript/webmio.js`


We will always update the read me file as the knowledge grows. Please help to keep the document updated with the knowledge you have.


## Module Logging

In Wisecp to create module logging we can use Modules class. It will work from every class where wisecp is loaded.

`
Modules::save_log($module_type,$module_name,$request,$response,$processed);
`

https://dev.wisecp.com/en/kb/creating-action-log-and-module-log

## Currency Conversion

In Wisecp we can convert currency id to currency code by using helpers. We have created a function `cid_convert_code` for example. which will convert the currency id to the code. WISECP must be loaded on the page to use it.

`
function cid_convert_code($id = 0)
{
    Helper::Load("Money");
    $currency   = Money::Currency($id);
    if ($currency) return $currency["code"];
    return false;
}
`

Currency Exchange
In Wisecp we can exchange the currency values by using currency codes.

`
Money::exChange($total_price, $original_currency, $currency_to_exchange)
`


## JSON Conversion

WISECP uses its own json conversion classes to convert the data into json and into json. 
`jencode` is used to convert into json and `jdecode` is used to convert from json.

`
Utility::jencode($php_array)
`
`
Utility::jdecode($json_data)
`



## Models

WISECP provides models to manipulate data in the tables. Here are the some of the modules with their methods to get and set the data.

`Invoices::get($invoiceId)` It will get the invoice by id and return in array.

`Invoices::set($invoiceId, ["status" => "paid","datepaid" => date("Y-m-d H:i:s")])` It will update the invoice.

`Money::getCurrencies()` It will returns all active currencies in WISECP.

`Money::formatter_symbol($amount, $currency)` It will format the amount with the given currency and returns the output.

`Config::get("theme/notifi-header-logo")` To get configuration stored in `coremio/configuration`. `theme` is the file name and `notifi-header-logo` is the key name. To look into deep array use `/` symbol for child.

`Hook::run("ClientAreaEndBody")` To trigger a hook to run.

`User::getData($user_id, $field_name, 'array')` It will return the user data. We can pass comma seperated string of field want to retrieve in $field_name



## Sweetalert on clientarea

WISECP provides Sweetalert to use by default on the clientarea. 

```
<script>
            swal({
                title: title,
                text: message,
                timer: 50000,
                type: 'error', // type can be success,error,warning
                showConfirmButton:false
            });
            setTimeout(function(){
            },1000);
        </script>

```


## Logged user info

WISECP provides logged member/admin info in UserManager Class functions.

`$type` can be `member` or `admin` 

To validate login we can use `UserManager::LoginCheck($type)`

To get logged in user info we can use `UserManager::LoginData($type)`. It will return array of data.

To get logged in user Ip address we can use `UserManager::GetIP()`. 


## Get Languages

WISECP provide all languages activated in WISECP and current active language in Language class

`$lang=new Language;` // Create a object of language class

`$allActivelanguages=$lang->rank_list()` will return list of all active languages in WISECP.

`$activelanguages=$lang->detect_lang()` will return the current langauge in use.

